(function () {
    var R = React;
    var D = React.DOM;
    var cmd = function () {
        return window.ispmanager_forward.cmd;
    };
    var utils = window.ispmanager_forward.utils;
    var ui = window.ispmanager_forward.components;
    var getLabel = utils.getLabel;
    var capitalize = utils.capitalize;
    var ImmutableRenderMixin = utils.ImmutableRenderMixin;

    /*
     * Components
     */

    ui.AddressRow = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;

            return (
                D.span(null, [
                    D.input({
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateAddress(that.props.idx, e.target.value);
                        }
                    }),
                    " ",
                    D.button({
                        onClick: function (e) {
                            e.preventDefault();
                            cmd().addAddressBelow(that.props.idx);
                        }
                    }, "+"),
                    " ",
                    D.button({
                        onClick: function (e) {
                            e.preventDefault();
                            cmd().removeAddress(that.props.idx);
                        }
                    }, "\u2013")
                ])
            );
        }
    });

    ui.AddressesField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "forward-addresses"}, capitalize(getLabel('addresses'))),
                    this.props.data.map(function (address, idx) {
                        return D.div({className: 'forward-address-row'},
                            R.createElement(ui.AddressRow, {data: address, idx: idx}))
                    })
                ])
            );
        }
    });

    ui.NoLocalField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({htmlFor: "forward-nolocal"}, capitalize(getLabel('nolocal'))),
                    D.input({
                        id: 'forward-nolocal',
                        type: 'checkbox',
                        checked: this.props.data,
                        onChange: function (e) {
                            cmd().updateNoLocal(e.target.checked);
                        }
                    })
                ])
            );
        }
    });

    ui.ForwardForm = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div(null, [
                    D.div({className: 'forward-field'},
                        R.createElement(ui.AddressesField, {data: this.props.data.get('addresses')})
                    ),
                    D.div({className: 'forward-field'},
                        R.createElement(ui.NoLocalField, {
                            data: this.props.data.get('noLocal'),
                            disabled: !this.props.data.get('active')
                        })
                    ),
                    D.div({className: "buttons"},
                        D.input({
                            type: 'button',
                            className: 'mainaction button',
                            value: capitalize(getLabel('savechanges')),
                            onClick: function (e) {
                                e.preventDefault();
                                cmd().persistForward();
                            }
                        })
                    )
                ])
            );
        }
    });

    ui.ForwardView = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({
                        id: "filter-box",
                        className: "uibox contentbox",
                        style: {left: 0, padding: "10px", overflowY: "scroll"}
                    },
                    R.createElement(ui.ForwardForm, this.props)
                )
            );
        }
    });
})();

