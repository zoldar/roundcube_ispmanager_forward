ISPManager forward rules API plugin for Roundcube
=================================================

A plugin for managing forward rules using ISPSystem's (https://www.ispsystem.com/) ISPmanager API.

ATTENTION
---------

That plugin is still WORK IN PROGRESS and is not supposed to be used in production.


Quick start
-----------

First, fetch the plugin to Roundcube's `plugins/` folder:

    git clone git@bitbucket.org:zoldar/roundcube_ispmanager_forward.git $ROUNDCUBE_HOME/plugins/ispmanager_forward

Next, copy `config.inc.php.dist` to `config.inc.php` and set proper ISPManager URL.

Finally, add `ispmanager_forward` to `$config['plugins']` array in the main config file under `$ROUNDCUBE_HOME/config/config.inc.php`.
    
License
-------
This plugin is released under the [GNU General Public License Version 3+](http://www.gnu.org/licenses/gpl.html).
