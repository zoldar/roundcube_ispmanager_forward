<?php

class rcube_forward_engine
{
    private $rc;
    private $plugin;
    private $api;
    private $currentUser;

    /**
     * Class constructor
     */
    function __construct($plugin)
    {
        $this->rc = rcube::get_instance();
        $this->currentUser = $this->resolveUser();
        $this->plugin = $plugin;
    }

    private function getAPI()
    {
        if (!$this->api) {
            $username = $this->rc->config->get('ispmanager_forward_username', '');
            $password = $this->rc->config->get('ispmanager_forward_password', '');
            $apiUrl = $this->rc->config->get('ispmanager_forward_api_url', 'https://localhost:1500/ispmgr');
            $debug = $this->rc->config->get('ispmanager_forward_debug', false);

            $this->api = new rcube_forward_api($username, $password, $apiUrl, $debug);
        }

        return $this->api;
    }

    function actions()
    {
        $params = rcube_utils::get_input_value('_params', rcube_utils::INPUT_GPC);
        $paramsArray = json_decode(urldecode($params), true);
        if ($paramsArray && !empty($paramsArray['func']) && $paramsArray['func'] == 'email.edit') {
            $cleanParams = $this->sanitizeParams($paramsArray);

            $response = $this->sanitizeResponse($this->getAPI()->query($cleanParams));

            $this->rc->output->command(
                'ispmanager_forward_handle_response', 'response', $response);
        } else {
            $this->send();
        }
    }

    function send()
    {
        $this->rc->output->set_pagetitle($this->plugin->gettext('forwards'));
        $this->rc->output->send('ispmanager_forward.manage');
    }

    private function sanitizeParams($params) {
        $output = array();
        $allowedParams = array('sok', 'forward', 'dontsave');

        foreach ($allowedParams as $allowedParam) {
            if (isset($params[$allowedParam])) {
                $output[$allowedParam] = $params[$allowedParam];
            }
        }

        $output['elid'] = $this->currentUser;
        $output['func'] = 'email.edit';

        return $output;
    }

    private function sanitizeResponse($response) {
        unset($response['doc']['passwd']);
        unset($response['doc']['doc']['passwd']);
        return $response;
    }

    private function resolveUser()
    {
        $user = $this->rc->config->get('sauserprefs_userid', "%u");

        $identity_arr = $this->rc->user->get_identity();
        $identity = $identity_arr['email'];
        $user = str_replace('%u', $_SESSION['username'], $user);
        $user = str_replace('%l', $this->rc->user->get_username('local'), $user);
        $user = str_replace('%d', $this->rc->user->get_username('domain'), $user);
        $user = str_replace('%i', $identity, $user);

        return $user;
    }
}
